package main;
import java.time.LocalDate;
import java.util.List;

/**
* A interface specifies the operations on Customer object
*
* @author nickson
*
*/
public interface PlayerService {
	
	/**
	* The method will add a player
	* @param player Player to be added
	 * @throws Exception 
	 * @throws  
	 * @throws ClassNotFoundException 
	*/
	 void add(Player player) throws Exception;
	 
	 /**
	 * The method should display all the player
	 * @throws Exception 
	 */
	 void display() throws Exception;
	 
	 /**
	 * The method will search player and display the player with given name
	 * @param name name of the player to be searched
	 * @throws Exception 
	 *
	 */
	 void search(String name) throws Exception;
	 
	 /**
	 * The method sort the players based on name
	 * @throws Exception 
	 */
	 void sort() throws Exception;
	 
	 /**
	 * The method will search players and display the player with dateofBirth is after the given dateofBirth
	 * @param dateofBirth
	 * @return 
	 * @throws Exception 
	 *
	 */
	 List<Player> search(LocalDate dateofBirth) throws Exception;
	 
	 /**
      * The default method will return data in players array to a string
	  * @param Player players
	 * @throws Exception 
	  *
	  */
	 void delete(String name) throws Exception;
	 
	 void download() throws Exception; 
	 default String csvConversion(Player players) {
		
	   return new StringBuffer().append("\"").append(players.getName()).append("\",\"").append(players.getTeam()).append("\",")
			  .append(players.getAge()).append(",\"").append(players.getDob()).append("\",\"")
		    		.append(players.getGender()).append("\"\n").toString();
	        }
	 void downloadJson() throws Exception;
	 default String asJson(Player players) {
		 return new StringBuffer().append("{").append("\"Name\":").append("\"").append(players.getName()).append("\"")
				 .append(",\"Team\":").append("\"").append(players.getTeam()).append("\"").append(",")
				 .append("\"Age\":").append(players.getAge()).append(",").append("\"Date of Birth\":")
				 .append("\"").append(players.getDob()).append("\"").append(",").append("\"Gender\":")
				 .append("\"").append(players.getGender()).append("\"").append("}").append("\n").toString();
	       }


           }

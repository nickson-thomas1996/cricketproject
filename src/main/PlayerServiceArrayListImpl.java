package main;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.Iterator;


public class PlayerServiceArrayListImpl implements PlayerService {
    int j=0;
	List<Player> playerList=new ArrayList<>();
	@Override
	public void add(Player player) {
		int j=0;
		if(playerList.size()>0&& playerList.contains(player)) {
			System.out.print("Already Exist");
		}
		else {
		playerList.add(player);
		}j++;
	}

	@Override
	public void display() {
		
		playerList.forEach(System.out::println);
	}

	@Override
	public void search(String name) {
		{
			playerList.stream().filter(player->player.getName().equalsIgnoreCase(name)).forEach(System.out::println);
            }
		}

	@Override
	public void sort() {
		Collections.sort(playerList);
		display();
	}

	@Override
	public List<Player> search(LocalDate dateofBirth) {
		return playerList.stream()
		.filter(playerList->playerList.getDob().isAfter(dateofBirth))
		.collect(Collectors.toList());
		
	}

	@Override
	public void download() throws Exception {
		FileWriter file = new FileWriter("download.csv");
		for (int i = 0; i < j; i++) {
			String data = csvConversion(playerList.get(i));
			file.write(data);
		}
		file.close();
	}

	@Override
	public void downloadJson() throws IOException {
		
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("download.json");
		for (Player player : playerList) {
		data = asJson(player);
		join.add(data);
		}file.write(join.toString());
		file.close();
	}

	@Override
	public void delete(String name) {

		Iterator<Player> iterator = playerList.iterator();
		while (iterator.hasNext()) {
		Player playerlist = (Player)iterator.next();
		for(int i=0; i<playerList.size();i++) {
		if (playerlist.getName().equals(name))
		{
		iterator.remove();
		System.out.println("Successfully deleted");
		}
		}}
//			for (Iterator<Player> iterator = playerList.iterator();iterator.hasNext();){
//			Player player = iterator.next();
//			if (player.getName().equals(name)){
//			iterator.remove();
//			}}
		
}
}

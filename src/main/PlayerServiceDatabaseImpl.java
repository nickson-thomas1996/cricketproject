package main;
import java.sql.Date;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class PlayerServiceDatabaseImpl implements PlayerService {
    String driver,url,user,password;
    
	public PlayerServiceDatabaseImpl(String driver, String url, String user, String password) {
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.password = password;
	
	}
    private Connection getConnection() throws ClassNotFoundException, SQLException{
    	Class.forName(driver);
    	return DriverManager.getConnection(url,user,password);
    }
   
    private Player getPlayer(ResultSet resultSet) throws Exception { 
    Player players = new Player();
    players.setName(resultSet.getString("name"));
    players.setTeam(resultSet.getString("team"));
    players.setAge(resultSet.getInt("age"));
    players.setDob(resultSet.getDate("dob").toLocalDate());
    players.setGender(Gender.valueOf(resultSet.getString("gender")));
	return players;
     }

	@Override
	public void add(Player player){
		
			try {
				Connection connection=getConnection();
				connection = getConnection();
				String query = "INSERT INTO players(name,team,age,dob,gender) VALUES(?,?,?,?,?)";
				PreparedStatement preparedStatement = connection.prepareStatement(query);
				preparedStatement.setString(1,player.getName());
				preparedStatement.setString(2,player.getTeam());
				preparedStatement.setInt(3,player.getAge());
				preparedStatement.setDate(4,Date.valueOf(player.getDob()));
				preparedStatement.setString(5,player.getGender().toString());
				preparedStatement.executeUpdate();
				connection.close();
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
	
	}

	@Override
	public void display() throws Exception {
    
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("SELECT * FROM players;");
			ResultSet resultSet=prestatement.executeQuery();
			
			while(resultSet.next())
			{  
				Player players=getPlayer(resultSet);
				System.out.println(players);

			}
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void search(String search) throws Exception {
    
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("SELECT * FROM players  where name=?");
			prestatement.setString(1,search);
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next())
			{
				Player players=getPlayer(resultSet);
				System.out.println(players);
			}
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		}
		
	}

	@Override
	public void sort() throws Exception {
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("SELECT * FROM players order by name");
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next())
			{
				Player players=getPlayer(resultSet);
				System.out.println(players);
			}
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		}
	    }

	@Override
	public List<Player> search(LocalDate dateofBirth) throws Exception {
		List<Player> playerList=new ArrayList<>();
		try {
		Connection connection = getConnection();
		connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("SELECT * FROM players where dob>?;");
		preparedStatement.setDate(1,Date.valueOf(dateofBirth));
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		playerList.add(getPlayer(resultSet));
		}
		connection.close(); } 
		catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
		}
		return playerList;
		}
	
	@Override
	public void delete(String name) throws Exception {
		
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("DELETE FROM players  where name=?");
			prestatement.setString(1,name);
			prestatement.executeUpdate();
			System.out.println("Data Deleted From DataBase");
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		}}

	@Override
	public void download() throws Exception {
		    Connection connection;
		    try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from players");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("PlayersData.csv");
			while (resultSet.next()) {
			String data = csvConversion(getPlayer(resultSet));
			file.write(data);
			}
			file.close();
			connection.close();
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}}

	@Override
	public void downloadJson() throws Exception {
		Connection connection;
		try {
			String data;
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from players");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter("PlayerDatabase.json");
		StringJoiner join = new StringJoiner(",", "[", "]");
		while (resultSet.next()) {
		data = asJson(getPlayer(resultSet));
		join.add(data);
		}file.write(join.toString());
		file.close();
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
		}

	}
}

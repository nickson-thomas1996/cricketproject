package main;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
public class Menu {
	public static void main(String[] args) throws Exception {
		int c;
		Scanner scanner=new Scanner(System.in);
		FileReader reader=new FileReader("C:\\Users\\Bourntec\\git\\javatraining\\src\\config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		PlayerService playerService=PlayerServiceFactory.getPlayerService("DataBase",properties);
//		PlayerService playerService=PlayerServiceFactory.getPlayerService("ArrayList",properties);
		do
			{
			System.out.println("Enter choice \n 1: add \n 2: display \n 3:Search \n 4:Sort \n 5:Search By Date Of Birth \n 6:Delete \n 7:Download \n 8:Download As Json \n 9:Exit");
			c=scanner.nextInt();
			switch(c)
			{
			case 1:
			{    try{ 
					System.out.println("Enter the name of player");
					String name=scanner.next();
				    System.out.println("Enter the team");
				    String team=scanner.next();
				    System.out.println("Enter age");
				    int age=scanner.nextInt();
				    System.out.println("Enter date In yyyy-mm-dd format");
				    String date=scanner.next();
				    LocalDate dateOfBirth=LocalDate.parse(date);
				    System.out.println("Enter Gender");
				    String gendervalue=scanner.next();
				    Gender gender=Gender.valueOf(gendervalue.toUpperCase());
				    Player player =new Player(name,team,age,dateOfBirth,gender);
				    playerService.add(player);
				    }
				    catch(InputMismatchException e){
					System.out.println(e.getMessage());
					scanner.nextLine();}
			    break;
			}
			case 2:
			{
			playerService.display();
	        break;
			}
			case 3:
			{   System.out.println("Enter Name of player To Be Searched\n");
		        String name=scanner.next();
		        playerService.search(name);
				break;
			}
			case 4:
			{
				playerService.sort();
				break;
			}
			case 5:
			{   System.out.println("Enter Dob of player To Be Searched\n");
		        String date=scanner.next();
		        LocalDate dateOfBirth=LocalDate.parse(date);
//		        playerService.search(dateOfBirth);
		        List<Player> Players=playerService.search(dateOfBirth);
		        System.out.println(Players);
		        if(Players==null)
		        System.out.print("No Data Found");
				break;
			}
			
			case 6:
			{   System.out.println("Enter Name of Player To be Deleted\n");
		        String name=scanner.next();
		        playerService.delete(name);
		        break;
			}
			
			case 7:
			{   try {
				playerService.download();
			} catch (Exception e) {
				e.printStackTrace();
			}
				break;
				
			}
			case 8:
			{   try {
				playerService.downloadJson();}
			    catch(Exception e) {
				e.printStackTrace();
			}
				break;
			} 
				
			case 9:
				System.out.println("Exit");
				break;
			}}while((c!=9));
		    scanner.close();
			}
            }

package main;
import java.io.FileWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class PlayerServiceArrayImpl implements PlayerService {
	private Player[] players;
	private int j = 0;

	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
	public PlayerServiceArrayImpl() {
		players=new Player[100];
	}
	public PlayerServiceArrayImpl(int size) {
		players=new Player[size];
	}
	
	/**
	 * The method will add a player using constructor
	 * 
	 * @param player
	 */
	public void add(Player player) {
		boolean flag = true;
		if (j >= players.length)
			System.out.println("Data Limit exceeded");
		else if (flag == true) {
			for (int i = 0; i < j; i++) {
				if (players[i].equals(player)) {
					System.out.println("Duplicate Data");
					flag = false;
				}
			}
		}
		if (flag == true) {
			players[j] = player;
			j++;
		}
	}

	/**
	 * The method will display players
	 */
	public void display() {
		if (j == 0) {
			System.out.println("No data To display");
		} else
			for (int i = 0; i < j; i++) {
				System.out.println(players[i]);
			}
	}

	/**
	 * The method will search for players
	 * 
	 * @param name
	 */
	public void search(String name) {
		boolean flag = false;
		for (int i = 0; i < j; i++) {
			if (players[i].getName().equalsIgnoreCase(name)) {
				System.out.println(players[i]);
				flag = true;
			}
		}
		if (flag == false)
			System.out.println("No data To display");
	}

	/**1
	 * The method will sort players
	 */
	public void sort() {
		Arrays.sort(players, 0, j);
		display();
	}

	/**
	 * The method will Search players Using Date funtion
	 * 
	 * @param dateOfBirth
	 */
	public List<Player> search(LocalDate dateOfBirth) {
			List<Player> playerList = new ArrayList<>();
			for (int i = 0; i < j; i++) {
			if (players[i].getDob().isAfter(dateOfBirth)) {
				playerList.add(players[i]);
			}}
		return playerList;
	}

	/**
	 * The method will download players data to a csv format file
	 * 
	 */
	public void download() throws Exception {

		FileWriter file = new FileWriter("download.csv");
		for (int i = 0; i < j; i++) {
			String data = csvConversion(players[i]);
			file.write(data);
		}
		file.close();
	}

	/**
	 * The method will download players data to a json format file
	 * 
	 */
	public void downloadJson() throws Exception{
		    String data;
			StringJoiner join=new StringJoiner(",","[","]");
			FileWriter fileWriter=new FileWriter("DonloadArray.json");
			for(int i=0;i<j;i++)
			{
				data=asJson(players[i]);
				join.add(data);
			}
			fileWriter.write(join.toString());
			fileWriter.close();
	}
	
	@Override
	public void delete(String name) {
			int k = 0;
			for (int i = 0; i < j; i++) {
			if (!players[i].getName().equals(name)) {
				players[k] = players[i];
			k++;
			System.out.println("deleted\n");
			}
			}j = k;
			}
	}

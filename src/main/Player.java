package main;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;
/**
* A class specifies the operations on Player object
* @author nickson
*/
public class Player implements Comparable<Player>{ 
		 private String name,team;
		 private int age;
		 private LocalDate dateOfBirth;
		 private Gender gender;
		 
/**
* The constructor will add a player
* @param name,team,age,dob,j
*/
	public Player(String name, String team,int age, LocalDate dob,Gender gender) 
	{   this.name=name;
		this.team=team;
		this.age=age;
		this.dateOfBirth=dob;
		this.gender=gender;
	}
	public Player() {
	}
	public String getName()
	{return name;}
	
	public void setName(String name) 
	{this.name=name;}
	
	public String getTeam()
	{return team;}
	
	public void setTeam(String team) 
	{this.team=team;}
	
	public int getAge()
	{return age;}
	
	public void setAge(int age) 
	{this.age=age;}
	
	public LocalDate getDob() 
	{return dateOfBirth;}
	
	public void setDob(LocalDate date) 
	{this.dateOfBirth = date;}
	
	/**
	* The method will override the toString method and return as String value
	* 
	*/
	public String toString()
	{  
		return new StringBuffer("Name :").append(name).append("\nTeam :").append(team)
				.append("\nAge :").append(age).append("\nDate Of Birth:").append(dateOfBirth)
				.append("\nGender:").append(gender).append("\n").toString();
	}
	
	/**
	* The method used to check for equals
	* 
	*/
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		return age == other.age && Objects.equals(dateOfBirth, other.dateOfBirth) && gender == other.gender
				&& Objects.equals(name, other.name) && Objects.equals(team, other.team);
	}
	/**
	* The method will used to sort using compareTo
	* 
	*/
	public int compareTo(Player players){
		return age-players.age;
	}
	
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	}
